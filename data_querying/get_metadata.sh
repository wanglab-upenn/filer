#!/bin/bash
set -e

filterString=${1:-"."} # jq filter string
genomeBuild=${2:-"hg19"}
configFile=${3:-"filer.ini"}


bashVer=${BASH_VERSINFO[0]}

if [ $# -lt 3 ]; then
  cat <<- HELP
Script: $(basename $0)
Summary: retrieve track metadata for a given genome build and track filter

USAGE: $0 <filter_string> <genome_build> <config_file>
	<filter_string> = jq track filter string. Example ."Data Source" == "DASHR2". Set to "." to retrieve all tracks.
	<genome_build> = hg19|hg38
	<config_file> = FILER config file

Examples:
bash $0 ".\"Identifier\" == \"NGEN000601\"" hg19 filer.ini
bash $0 ".\"Data Source\" == \"DASHR2\"" hg38 filer.ini 
bash $0 ".\"Data Source\" == \"ENCODE\" and .\"cell type\" == \"CD14+ monocyte\" " hg19 filer.ini > out.metadata.json
HELP
	exit 1
fi

source "${configFile}"
metadataFile="${FILERMETADATA}"
MLR="${MLR}"
JQ="${JQ}"
[ ! -x "${MLR}" ] && { echo "***ERROR: mlr not found"; exit 1; }
[ ! -x "${JQ}" ] && { echo "***ERROR: jq not found"; exit 1; }

"${MLR}" --icsv --fs tab --rs lf --ojson --jlistwrap cat "${metadataFile}" | "${JQ}" '[ .[] | select( ."Genome build" == "'"${genomeBuild}"'" and ('"${filterString}"') ) ]'

